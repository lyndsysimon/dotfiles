local use = require('packer').use

return require('packer').startup(function()
  -- packer can manage itself
  use 'wbthomason/packer.nvim'

  use 'mileszs/ack.vim'

  -- slightly fancier statusline
  use {
    'hoob3rt/lualine.nvim',
    required = { 'kyazdani42/nvim-web-devicons', opt = true },
  }

  -- color scheme
  use {"metalelf0/jellybeans-nvim", requires = {"rktjmp/lush.nvim"}}

  use {
    -- configs for built-in LSP client
    'neovim/nvim-lspconfig',
    -- automatic installation of LSPs
    'kabouzeid/nvim-lspinstall',
  }

  -- autocompletion
  use 'hrsh7th/nvim-compe'

  -- key mapping
  use 'folke/which-key.nvim'

  --
  -- Below this line, I'm not yet happy with the config
  --

  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }

  -- add git change indicators to gutter
  use {'lewis6991/gitsigns.nvim', requires={
    {'nvim-lua/plenary.nvim'}
  }}

  -- UI
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      {'nvim-lua/popup.nvim'},
      {'nvim-lua/plenary.nvim'}
    }
  }
end)
