local o = vim.o
local wo = vim.wo
local g = vim.g
local cmd = vim.cmd


-- use spacebar as leader key
vim.api.nvim_set_keymap('n', '<Space>', '', {})
g.mapleader = ' '
g.maplocalleader = ' '

o.termguicolors = true
-- TODO: This will fail if the colorscheme isn't installed...
cmd([[colorscheme jellybeans-nvim]])

o.clipboard = 'unnamedplus'

o.updatetime = 300

o.showmode = false

o.expandtab = true
o.tabstop = 2
o.softtabstop = 2
o.shiftwidth = 2
o.smartindent = true
o.autoindent = true

g.indent_blankline_char = "┊"
g.indent_blankline_filetype_exclude = { 'help', 'packer' }
g.indent_blankline_buftype_exclude = { 'terminal', 'nofile'}
g.indent_blankline_char_highlight = 'LineNr'

o.foldlevelstart = 10
if pcall(require, 'nvim-treesitter') then
  o.foldmethod = "expr"
  o.foldexpr = "nvim_treesitter#foldexpr()"
else
  o.foldmethod = "manual"
end

o.incsearch = true
o.inccommand = "nosplit"
o.hlsearch = true
o.ignorecase = true
o.smartcase = true

o.splitbelow = true
o.splitright = true
o.hidden = true
wo.signcolumn = "yes"
cmd 'set list lcs=tab:>-,eol:¬,trail:▒'

o.showcmd = true

o.backup = false

wo.number = true
