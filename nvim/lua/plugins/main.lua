local use = require('packer').use

require('packer').startup(function()
  -- package manager
  use 'wbthomason/packer.nvim'

  -- slightly fancier statusline
  use 'itchyny/lightline.vim'

  -- configs for built-in LSP client
  use 'neovim/nvim-lspconfig'

  -- autocompletion
  use 'hrsh7th/nvim-compe'

  -- Indentation guides, even on blank lines
  -- use { 'lukas-reineke/indent-blankline.nvim', branch="lua" }

  --
  -- Below this line, I'm not yet happy with the config
  --

  use 'nvim-treesitter/nvim-treesitter'

  -- UI for selecting things
  use {'nvim-telescope/telescope.nvim', requires={
    {'nvim-lua/popup.nvim'},
    {'nvim-lua/plenary.nvim'}
  }}

  -- add git change indicators to gutter
  use {'lewis6991/gitsigns.nvim', requires={
    {'nvim-lua/plenary.nvim'}
  }}
end)
