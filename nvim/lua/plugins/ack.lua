local eval = vim.api.nvim_eval
local g =vim.g 

if eval("executable('rg')") then
  g.ackprg = 'rg --vimgrep --no-heading'
else
  print("ripgrep not install; Ack.vim is unavailable")
end
