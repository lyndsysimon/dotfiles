local map = vim.api.nvim_set_keymap

require('telescope').setup{

}

options = { noremap = true }
map('n', '<leader>1', ':Telescope file_browser<CR>', options)
map('n', '<leader>b', ':Telescope buffers<CR>', options)
