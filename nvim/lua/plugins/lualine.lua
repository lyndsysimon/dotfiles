local function ts_status()
  return require'nvim-treesitter'.statusline(90)
end

require'lualine'.setup{
  options = {
    theme = 'jellybeans',
  },
  sections = {
    lualine_x = {ts_status, 'filetype'},
  },
}
