vim.g.lightline = {
  active = {
    left = {
      { 'mode', 'paste' },
      { 'ts_component', 'readonly', 'filename', 'modified' }
    }
  };
  component_function = { ts_component = 'nvim_treesitter#statusline', };
}
