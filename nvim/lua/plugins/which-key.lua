local wk = require('which-key')

wk.register({
  -- exit insert mode with "jj"
  ["jj"] = { "<Esc>", "Exit insert mode", mode="i" },

  ['<leader>'] = {
    f = { "<cmd>Telescope live_grep<cr>", "Search workspace for a string" },
    h = { "<cmd>nohlsearch<cr>", "Remove search highlights" },
  },

  -- movement based on visual lines (respects wrapping)
  j = { "gj", "Move down" },
  k = { "gk", "Move up" },

  -- move between splits
  ["<C-H>"] = { "<C-W><C-H>", "Move one split left" },
  ["<C-J>"] = { "<C-W><C-J>", "Move one split down" },
  ["<C-K>"] = { "<C-W><C-K>", "Move one split up" },
  ["<C-L>"] = { "<C-W><C-L>", "Move one split right" },
})
