ZSH configuration
=================

Config files, in the order in which they are called:

1. ``.zshenv``
2. ``.zprofile`` (if login)
3. ``.zshrc`` (if interactive)
4. ``.zlogin`` (if login)
5. ``.zlogout`` (if login, unless terminated by an ``exec``)

More info: http://zsh.sourceforge.net/Doc/Release/Files.html

Note that my ``.zshrc`` sources other files from this directory.
