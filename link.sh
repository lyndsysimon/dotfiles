# Link files in dotfiles to the locations where they are expected to be
dir=${PWD}

# Zsh
ln -si $dir/zsh/zshrc ~/.zshrc
ln -si $dir/zsh/zshenv ~/.zshenv
# "Pure" prompt
ln -si $dir/zsh/pure/pure.zsh $dir/zsh/functions/prompt_pure_setup
ln -si $dir/zsh/pure/async.zsh $dir/zsh/functions/async

# System-accessible custom scripts
ln -si $dir/bin ~/.bin

# Git
ln -si $dir/git/gitconfig ~/.gitconfig

# tmux
ln -si $dir/tmux/tmux.conf ~/.tmux.conf

# Tmuxinator
ln -si $dir/tmuxinator ~/.tmuxinator

# Hyper
#   Note: Removing the file and linking in a single command is required if
#   running as your shell, otherwise it'll reload and recreate the file, causing
#   the linking to fail.
rm -f ~/.hyper.js && ln -si $dir/hyper/hyper.js ~/.hyper.js

# SSH keys
ln -si $dir/ssh/authorized_keys ~/.ssh/authorized_keys
